from flask import Flask, render_template, request, redirect, url_for, session, escape
import json

app = Flask(__name__)

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/about/')
def about():
  return render_template('about.html')

#electric
@app.route('/electric/')
def electric():
  return render_template('electric/electric.html')

#Fenders
@app.route('/electric/fender/')
def fender():
  return render_template('electric/fender/fender.html')

@app.route('/electric/fender/stratocaster/')
def fender_stratocaster():
    model = "stratocaster"
    return display_json(model)

@app.route('/electric/fender/telecaster/')
def fender_telecaster():
    model = "telecaster"
    return display_json(model)


@app.route('/electric/fender/jaguar/')
def fender_jaguar():
  model = "jaguar"
  return display_json(model)

#Gibson
@app.route('/electric/gibson/')
def gibson():
  return render_template('electric/gibson/gibson.html')


@app.route('/electric/gibson/les_paul/')
def gibson_les_paul():
  model = "les-paul"
  return display_json(model)

@app.route('/electric/gibson/sg/')
def gibson_sg():
  model = "sg"
  return display_json(model)

@app.route('/electric/gibson/es_335/')
def gibson_es_335():
  model = "es-335"
  return display_json(model)


#Rickenbacker
@app.route('/electric/rickenbacker/')
def rickenbacker():
  return render_template('electric/rickenbacker/rickenbacker.html')

@app.route('/electric/rickenbacker/360_12/')
def rickenbacker360_12():
  model = "360-12"
  return display_json(model)

@app.route('/electric/rickenbacker/330/')
def rickenbacker_335():
  model = "330"
  return display_json(model)

@app.route('/acoustic/')
def acoustic():
   return render_template('acoustic/acoustic.html')

@app.route('/acoustic/martin/')
def martin():
   return render_template('acoustic/martin/martin.html')

@app.route('/acoustic/martin/d_28')
def martin_d_28():
   model = "d-28"
   return display_json(model)


#guitarist search
@app.route('/guitarist/')
@app.route('/guitarist/<name>')
def guitarist_search(name=None):
  if name == '':
    return render_template('about.html')
  else:
    with open('static/json/data.json') as data_file:
      data = json.load(data_file)
    return render_template('guitaristsearch.html', name=name, data=data)

#Return JSON data
def display_json(guitar):
    with open('static/json/data.json') as data_file:
      data = json.load(data_file)

    for guitars in data['guitars']:
        if(guitars['model'] == guitar):
           make = guitars['make']
           model= guitars['model']
           built = guitars['from']
           type_of_guitar = guitars['type']

    return render_template('guitarpage.html', make=make, model=model, built=built, type_of_guitar=type_of_guitar )


#Add new guitar
@app.route('/add/', methods=['GET', 'POST'])
def add():
    if request.method == 'POST':
        print request.form
        with open('static/json/data.json') as data_file:
         data = json.load(data_file)
         make = request.form['make']
         model = request.form['model']
         built = request.form['built']
         type_of_guitar = request.form['type']
         entry = {'make': make, 'model': model, 'type':type_of_guitar, 'from': built}

         data['guitars'].append(entry)

         with open('static/json/data.json', 'w') as new_file:
           json.dump(data, new_file, indent=4, sort_keys=True)

         return "Record for %s has been added" % model
    else:
        return render_template('add.html')

#error handlers
@app.errorhandler(404)
def page_not_found(error):
  return render_template('errors/errorhandler.html'), 404


@app.route('/login/', methods=['GET', 'POST'])
def login():
  if request.method == 'POST':
      session['username'] = request.form['username']
      return redirect(url_for('user'))
  else:
      return render_template('userlogin.html')

@app.route('/user/')
def user():
    if 'username' in session:
        username = session.get('username', None)
        return render_template('userpage.html', name=username)
    return redirect(url_for('login'))

@app.route('/logout/')
def logout():
    # remove the username from the session if it's there
    session.pop('username', None)
    return redirect(url_for('index'))

app.secret_key = '!\xdf\x1f\xd0\xd5W\xae}\xb6\t\x0f\x98\xee\xd3=\xf3\x1b\xa2E\x19\xc4K\xf0\xa0'

if __name__ == '__main__':
   app.run(host='0.0.0.0', debug=True)
